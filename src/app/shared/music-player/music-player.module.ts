import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MusicPlayerComponent } from './music-player.component';
import { MusicBarComponent } from './music-bar/music-bar.component'



@NgModule({
  declarations: [
    MusicPlayerComponent,
    MusicBarComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    MusicPlayerComponent
  ]
})
export class MusicPlayerModule { }
